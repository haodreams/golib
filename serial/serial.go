package serial

import (
	"errors"
	"fmt"
	"io"
	"log"
	"runtime"
	"strconv"
	"strings"
	"time"

	goserial "github.com/jacobsa/go-serial/serial"
)

// Serial 串口
type Serial struct {
	opt goserial.OpenOptions
	//Connect     string //linux 下 COM 会自动替换为/dev/ttyO
	maxBufSize  int //保留缓冲数据的最大数量， 至少ReadBufSize 的2倍
	ReadBufSize int //读取数据的缓冲区
	conn        io.ReadWriteCloser
	buf         sb
	err         error
	HaveRecv    bool //是否开启数据接收线程
	isRunning   bool
	eventChan   chan byte
}

func NewSerial(opts ...Option) *Serial {
	m := new(Serial)
	m.ReadBufSize = 512
	m.maxBufSize = 2048
	m.HaveRecv = true
	for _, f := range opts {
		f(m)
	}
	if m.maxBufSize < m.ReadBufSize*2 {
		m.maxBufSize = m.ReadBufSize * 2
	}
	m.buf.b.SetDisableEOF()
	m.eventChan = make(chan byte, 15)
	m.buf.b.SetData(make([]byte, m.maxBufSize))
	return m
}

func (m *Serial) OptionString() string {
	parity := "N"
	switch m.opt.ParityMode {
	case goserial.PARITY_NONE:
		parity = "N"
	case goserial.PARITY_ODD:
		parity = "O"
	case goserial.PARITY_EVEN:
		parity = "E"
	}
	return fmt.Sprintf("%s,%d,%s,%d,%d", m.opt.PortName, m.opt.BaudRate, parity, m.opt.DataBits, m.opt.StopBits)
}

// Closed 串口是否已经关闭
func (m *Serial) Closed() bool {
	return !m.isRunning
}

func (m *Serial) read() {
	buf := make([]byte, m.ReadBufSize)
	for m.isRunning {
		if m.conn == nil {
			break
		}
		if m.buf.Len() < m.ReadBufSize {
			m.eventChan <- 1 //打满缓冲区满，阻塞线程运行
			continue
		}

		n, err := m.conn.Read(buf)
		if err != nil {
			if err.Error() != "EOF" {
				m.err = err
				log.Println(err)
				time.Sleep(time.Second)
				continue
			}
		}
		if n > 0 {
			m.buf.Write(buf[:n])
			if len(m.eventChan) == 0 { //如果缓冲区没数据，通知缓冲区有数据
				m.eventChan <- 1
			}
		}
	}
}

// Open 打开
func (m *Serial) Open() (err error) {
	m.conn, err = goserial.Open(m.opt)
	if err != nil {
		return
	}
	if m.conn == nil {
		err = fmt.Errorf("'%s' open failed", m.OptionString())
		return
	}
	m.buf.Reset()
	m.err = nil
	m.isRunning = true
	if m.HaveRecv {
		go m.read()
	}
	return
}

// Close 关闭
func (m *Serial) Close() (err error) {
	m.isRunning = false
	if m.conn != nil {
		err = m.conn.Close()
		m.conn = nil
	}
	return
}

// Write 写数据
func (m *Serial) Write(p []byte) (n int, err error) {
	if m.conn == nil {
		err = errors.New("not connected")
		return
	}

	n, err = m.conn.Write(p)
	return
}

// Read 读数据
func (m *Serial) Read(p []byte) (n int, err error) {
	defer func() {
		//清空事件
		for len(m.eventChan) > 0 {
			<-m.eventChan
		}
	}()

	if m.conn == nil {
		err = errors.New("not connected")
		return
	}
	if m.err != nil {
		return 0, m.err
	}

	if m.buf.Len() > 0 {
		return m.buf.Read(p)
	}
	select {
	case <-time.After(time.Second):
		if m.buf.Len() > 0 {
			return m.buf.Read(p)
		}
	case <-m.eventChan:
		return m.buf.Read(p)
	}

	return
}

// Len 获取buf 数据的长度
func (m *Serial) Len() int {
	return m.buf.Len()
}

// Reset 清空数据
func (m *Serial) Reset() {
	m.buf.Reset()
}

// ReadTimeout 读取数据直到超时
func ReadTimeout(conn io.Reader, data []byte, msTimeout int) (num int, err error) {
	n := 0
	num = 0
	length := len(data)
	t := time.Now()
	for num < length && msTimeout > 0 {
		n, err = conn.Read(data[num:])
		if err != nil {
			return num, err
		}
		if n == 0 {
			t1 := int(time.Since(t) / time.Millisecond)
			if t1 >= msTimeout {
				err = errors.New("timeout")
				break
			}
		}
		//  else if t1+20 > msTimeout {
		// 		time.Sleep(time.Duration(msTimeout-t1) * time.Millisecond)
		// 	} else {
		// 		time.Sleep(time.Millisecond * 20)
		// 	}
		// 	continue
		// }
		num += n
	}
	return
}

type Option func(*Serial)

// 设置连接字符串
func WithConnect(conn string) Option {
	return func(m *Serial) {
		if len(conn) > 3 {
			if runtime.GOOS == "windows" {
				if strings.HasPrefix(conn, "/dev/tty") {
					pos := strings.IndexFunc(conn, func(c rune) bool {
						if c >= '0' && c <= '9' {
							return true
						}
						return false
					})
					if pos > 0 {
						conn = "COM" + conn[pos:]
					}
				}
			}
		}
		m.opt.InterCharacterTimeout = 10000 //ms
		m.opt.BaudRate = 9600
		m.opt.ParityMode = goserial.PARITY_NONE
		m.opt.DataBits = 8
		m.opt.StopBits = 1

		opt := strings.Replace(conn, " ", "", -1)
		s := strings.Split(opt, ",")
		if len(s) < 1 {
			panic("串口选项配置错误")
		}
		m.opt.PortName = s[0]
		if len(s) > 1 {
			ival, err := strconv.Atoi(s[1])
			if err != nil {
				return
			}
			m.opt.BaudRate = uint(ival) //9600
		}
		if len(s) > 2 {
			switch s[2] {
			case "n", "N":
				m.opt.ParityMode = goserial.PARITY_NONE
			case "e", "E":
				m.opt.ParityMode = goserial.PARITY_EVEN
			case "o", "O":
				m.opt.ParityMode = goserial.PARITY_ODD
			}
		}

		if len(s) > 3 {
			ival, err := strconv.Atoi(s[3])
			if err != nil {
				panic("DataBits error," + err.Error())
			}
			m.opt.DataBits = uint(ival)
		}

		if len(s) > 4 {
			switch s[4] {
			case "1":
				m.opt.StopBits = 1
			case "2":
				m.opt.StopBits = 2
			}
		}
		if len(s) > 5 {
			timeout, err := strconv.Atoi(s[5])
			if err != nil {
				return
			}
			if timeout > 100 {
				if timeout > 60000 {
					timeout = 60000
				}
			} else {
				timeout = 100
			}
			m.opt.InterCharacterTimeout = uint(timeout)
		}
	}
}

// 设置缓冲区大小
func WithBufferMaxSize(size int) Option {
	return func(m *Serial) {
		if size < 512 {
			size = 512
		}
		m.maxBufSize = size
	}
}

// 设置读取缓冲区的大小
func WithReadBufferSize(size int) Option {
	return func(m *Serial) {
		if size < 256 {
			size = 256
		}
		m.ReadBufSize = size
	}
}
