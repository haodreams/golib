/*
 * @Author: fuzhuang
 * @Date: 2023-12-11 09:23:21
 * @LastEditTime: 2024-11-20 09:12:19
 * @LastEditors: wangjun haodreams@163.com
 * @Description:
 * @FilePath: \golib\serialer\test\test.go
 */
package main

import (
	"bufio"
	"log"
	"time"

	"gitee.com/haodreams/golib/serial"
)

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	com := serial.NewSerial(serial.WithConnect("/dev/ttyO2,9600,N,8,1"))
	err := com.Open()

	// //	serial, err := serialer.Open("/dev/ttyO2,9600,N,8,1")
	// serial, err := goserial.Open("/dev/ttyUSB0,9600,N,8,1")
	if err != nil {
		log.Fatalln(err)
	}

	go func() {
		buf := bufio.NewReader(com)
		for {
			line, err := buf.ReadString('\n')
			if err != nil {
				continue
			}
			log.Println(line)
		}
	}()
	for {
		//		n, err := sock.Read(serial, data[:1])
		//		if err != nil {
		//			continue
		//		}
		//		if n > 0 {
		//			if data[0] == 'w' {
		//				serial.Write([]byte(time.Now().String() + "\n"))
		//			}
		//		}
		com.Write([]byte(time.Now().String() + "\n"))
		time.Sleep(time.Millisecond * 500)
	}

	//	time.AfterFunc(time.Second, func() {

	//	})
	//	log.Println("begin")
	//	n, err := sock.ReadTimeout(serial, data, 1000)
	//	//n, err := serial.Read(data)
	//	if err != nil {
	//		log.Fatalln(err)
	//	}
	//	log.Println(n)

}
