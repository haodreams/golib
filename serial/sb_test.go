package serial

import (
	"log"
	"testing"
)

func TestSB(t *testing.T) {
	b := make([]byte, 100, 200)
	log.Println(len(b), cap(b))
	b = b[100:]
	log.Println(len(b), cap(b))
	b = b[:]
	log.Println(len(b), cap(b))
}
