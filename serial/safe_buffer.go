package serial

import (
	"sync"

	"gitee.com/haodreams/libs/easy"
)

// sb Tread safe
type sb struct {
	b  easy.LoopBuffer
	rw sync.Mutex
}

// Read read
func (b *sb) Read(p []byte) (n int, err error) {
	b.rw.Lock()
	defer b.rw.Unlock()
	return b.b.Read(p)
}

// Write write
func (b *sb) Write(p []byte) (n int, err error) {
	b.rw.Lock()
	defer b.rw.Unlock()
	return b.b.Write(p)
}

// Reset reset
func (b *sb) Reset() {
	b.rw.Lock()
	defer b.rw.Unlock()
	b.b.Reset()
}

// Len len
func (b *sb) Len() int {
	b.rw.Lock()
	defer b.rw.Unlock()
	return b.b.AvailWrite()
}
