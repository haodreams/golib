/*
 * @Author: Wangjun
 * @Date: 2021-08-09 16:33:05
 * @LastEditTime: 2021-08-09 16:47:21
 * @LastEditors: Wangjun
 * @Description:内存文件映射
 * @FilePath: \golib\memfile\file.go
 * hnxr
 */

package memfile

import (
	"gitee.com/haodreams/golib/lz4file"
	"github.com/ugorji/go/codec"
)

/**
 * @description:
 * @param {string} filePath
 * @param {interface{}} memdata //必须是一个地址
 * @return {*}
 */
func Load(filePath string, memdata interface{}) (err error) {
	data, err := lz4file.LoadLz4File(filePath)
	if err != nil {
		return
	}
	mh := new(codec.MsgpackHandle)
	err = codec.NewDecoderBytes(data, mh).Decode(memdata)
	if err != nil {
		return
	}
	return
}

/**
 * @description:
 * @param {string} filePath
 * @param {interface{}} memdata //必须是一个地址
 * @return {*}
 */
func Save(filePath string, memdata interface{}) (err error) {
	mh := new(codec.MsgpackHandle)
	var data []byte
	err = codec.NewEncoderBytes(&data, mh).Encode(memdata)
	if err != nil {
		return
	}

	err = lz4file.SaveLz4File(filePath, data)
	if err != nil {
		return
	}
	return
}
