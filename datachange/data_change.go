/*
 * @Author: Wangjun
 * @Date: 2022-01-06 16:28:05
 * @LastEditTime: 2022-01-07 17:49:28
 * @LastEditors: Wangjun
 * @Description:
 * @FilePath: \golib\datachange\data_change.go
 * hnxr
 */

package datachange

type Memory struct {
	Key   string //主键
	Value int64  //变化的值
	Time  int64  //变化时的时间
}

/**
 * @description: 数据改变的回调服务
 * @param {*}
 * @return {*}
 */
type DataChanger interface {
	OnDataChange(dc *DataChange, now, currentValue int64)
}

type DataChange struct {
	ID     int //ID 识别码
	Memory     //内存数据

	Relation DataChanger //关联接口
}

/**
 * @description: 填入数据
 * @param {int64} now 当前时间
 * @param {int64} value
 * @param {int64} force 是否强制推送
 * @return {*}
 */
func (m *DataChange) Push(now int64, value int64, force bool) {
	if value == m.Value {
		if force {
			yesterday := now - 86400
			if m.Time < yesterday {
				m.Time = yesterday
			}
			m.Relation.OnDataChange(m, now, value)
			m.Time = now
		}
		return
	}

	if m.Relation != nil {
		m.Relation.OnDataChange(m, now, value)
	}

	m.Value = value
	m.Time = now
}
