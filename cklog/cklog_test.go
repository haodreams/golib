/*
 * @Author: Wangjun
 * @Date: 2023-06-29 10:46:55
 * @LastEditTime: 2023-06-29 10:58:20
 * @LastEditors: Wangjun
 * @Description:
 * @FilePath: \golib\cklog\cklog_test.go
 * hnxr
 */
package cklog

import (
	"testing"
	"time"
)

func TestLog(t *testing.T) {
	Setup("tcp://10.202.10.174:9000?database=hnxr&username=default&password=E35E00DF%23&compress=0")
	Log(1, "test1", "test data1")
	time.Sleep(time.Second * 4)
	Log(1, "test2", "test data2")
	Close()
	time.Sleep(time.Second * 1)
}
