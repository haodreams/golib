package main

import (
	"flag"
	"time"

	"gitee.com/haodreams/golib/ntp"
)

var plocal = flag.Bool("local", false, "local time")

func main() {
	flag.Parse()
	ntps := []string{"ntp.ntsc.ac.cn:123", "0.pool.ntp.org:123", "1.pool.ntp.org:123", "2.pool.ntp.org:123", "3.pool.ntp.org:123"}
	for _, n := range ntps {
		tm, err := ntp.GetNetworkTime(n)
		if err != nil {
			continue
		}
		if *plocal {
			now := tm.UTC().In(time.Local)
			SetTime(now)
		} else {
			SetTime(*tm)
		}
		break
	}
}
