/*
 * @Author: Wangjun
 * @Date: 2021-05-15 22:43:19
 * @LastEditTime: 2022-08-08 15:46:47
 * @LastEditors: Wangjun
 * @Description:
 * @FilePath: \golib\calc\init.go
 * hnxr
 */
package calc

import "github.com/Knetic/govaluate"

//Setup 初始化
func Setup(m map[string]govaluate.ExpressionFunction) {
	//日期函数
	m["Now"] = Now
	m["Today"] = Today
	m["TodayTime"] = TodayTime
	m["YearDay"] = YearDay
	m["Year"] = Year
	m["Month"] = Month
	m["Day"] = Day
	m["Hour"] = Hour
	m["Minute"] = Minute
	m["Second"] = Second

	//统计函数
	m["Avg"] = Avg
	m["Sum"] = Sum
	m["Max"] = Max
	m["Min"] = Min
}
