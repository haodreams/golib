package calc

import (
	"time"
)

//Now 返回当前时间
func Now(args ...interface{}) (interface{}, error) {
	return float64(time.Now().Unix()), nil
}

//YearDay 返回当前时间日 [1-365]
func YearDay(args ...interface{}) (interface{}, error) {
	return float64(time.Now().YearDay()), nil
}

//Today 返回当前日期
func Today(args ...interface{}) (interface{}, error) {
	now := time.Now()
	_, zone := now.Zone()
	seconds := now.Unix() + int64(zone)
	seconds = seconds - seconds%86400
	return float64(seconds), nil
}

//TodayTime 今天的时间
func TodayTime(args ...interface{}) (interface{}, error) {
	now := time.Now()
	_, zone := now.Zone()
	seconds := now.Unix() + int64(zone)
	seconds = seconds % 86400
	return float64(seconds), nil
}

//Year 返回当前时间年份
func Year(args ...interface{}) (interface{}, error) {
	return float64(time.Now().Year()), nil
}

//Month 返回当前时间月份 [1-12]
func Month(args ...interface{}) (interface{}, error) {
	return float64(time.Now().Month()), nil
}

//Day 返回当前时间日   [0-31]
func Day(args ...interface{}) (interface{}, error) {
	return float64(time.Now().Day()), nil
}

//Hour 返回当前时间小时 [0-24]
func Hour(args ...interface{}) (interface{}, error) {
	return float64(time.Now().Hour()), nil
}

//Minute 返回当前时间分  [0-59]
func Minute(args ...interface{}) (interface{}, error) {
	return float64(time.Now().Minute()), nil
}

//Second 返回当前时间秒 [0-59]
func Second(args ...interface{}) (interface{}, error) {
	return float64(time.Now().Second()), nil
}
