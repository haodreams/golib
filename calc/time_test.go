package calc

import (
	"log"
	"testing"
	"time"
)

func TestTimeZone(t *testing.T) {
	log.Println(time.Now().Zone())
	log.Println(time.Now().Local().Zone())
}
