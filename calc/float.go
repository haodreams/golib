/*
 * @Author: Wangjun
 * @Date: 2021-05-15 22:43:19
 * @LastEditTime: 2022-08-08 15:47:03
 * @LastEditors: Wangjun
 * @Description:
 * @FilePath: \golib\calc\float.go
 * hnxr
 */
package calc

import (
	"errors"
	"strconv"
	"time"
)

//Float64 获取float数据
func Float64(arg interface{}) (fval float64, err error) {
	switch f := arg.(type) {
	case bool:
		if f {
			fval = 1
		} else {
			fval = 0
		}
	case float32:
		fval = float64(f)
	case float64:
		fval = f
	case int:
		fval = float64(f)
	case int8:
		fval = float64(f)
	case int16:
		fval = float64(f)
	case int32:
		fval = float64(f)
	case int64:
		fval = float64(f)
	case uint:
		fval = float64(f)
	case uint8:
		fval = float64(f)
	case uint16:
		fval = float64(f)
	case uint32:
		fval = float64(f)
	case uint64:
		fval = float64(f)
	case string:
		fval, err = strconv.ParseFloat(f, 64)
	case time.Time:
		f.Unix()
	default:
		err = errors.New("not a number")
	}
	return
}
