/*
 * @Author: Wangjun
 * @Date: 2021-12-10 10:50:05
 * @LastEditTime: 2021-12-11 17:00:07
 * @LastEditors: Wangjun
 * @Description:
 * @FilePath: \golib\easyjson\json_test.go
 * hnxr
 */
package easyjson

import (
	"log"
	"testing"
	"unsafe"

	jsoniter "github.com/json-iterator/go"
)

func TestCode(t1 *testing.T) {
	type Test struct {
		Float float64
	}

	Default()

	jsoniter.RegisterTypeEncoderFunc("float64", func(ptr unsafe.Pointer, stream *jsoniter.Stream) {
		f := *((*float64)(ptr))
		stream.WriteRaw(FloatToString(f, 6))
	}, nil)

	t := new(Test)
	t.Float = 10000000000000.34 //float64(math.NaN())
	//
	json := jsoniter.ConfigCompatibleWithStandardLibrary
	d, err := json.Marshal(t)
	log.Println(string(d), err)
}
