/*
 * @Author: Wangjun
 * @Date: 2021-05-15 22:43:19
 * @LastEditTime: 2023-09-12 17:28:54
 * @LastEditors: Wangjun
 * @Description:
 * @FilePath: \golib\config\dbconf\confdb.go
 * hnxr
 */
package dbconf

import (
	"errors"
	"sort"

	"gitee.com/haodreams/libs/config"
	"gorm.io/gorm"
)

type App struct {
	DB   *gorm.DB
	Name string
}

// Conf .
type Conf struct {
	db *gorm.DB
	config.Conf
}

// Load 加载配置来自文件
func Load(db *gorm.DB, appName string) (conf config.Configer, err error) {
	c := new(Conf)
	app := new(App)
	app.DB = db
	app.Name = appName
	conf, err = c.Load(app)
	c.SetDriverName("db")
	return
}

// Load 从数据库加载配置
func (m *Conf) Load(file interface{}) (conf config.Configer, err error) {
	app, ok := file.(*App)
	if !ok {
		return nil, errors.New("无效的参数")
	}
	if app.DB == nil {
		return nil, errors.New("数据库对象空指针")
	}

	conf = m
	m.db = app.DB

	//自动建表
	m.db.AutoMigrate(new(config.KV))

	var kvs []*config.KV
	err = m.db.Find(&kvs).Error
	if err != nil {
		return
	}
	m.SetList(kvs)
	return
}

// Save 保存到关系库
func (m *Conf) Save(file ...interface{}) (err error) {
	if len(m.List()) == 0 {
		return
	}

	var ok bool
	var db = m.db
	if len(file) > 0 {
		if db, ok = file[0].(*gorm.DB); !ok {
			return errors.New("无效的参数")
		}
		if db == nil {
			return errors.New("数据库对象空指针")
		}
	}

	list := m.List()
	sort.Slice(list, func(i, j int) bool {
		return list[i].Name < list[j].Name
	})

	err = db.Transaction(func(tx *gorm.DB) error {
		for _, row := range list {
			err = tx.Save(row).Error
		}
		return nil
	})

	return
}
