/*
 * @Author: wangjun haodreams@163.com
 * @Date: 2024-11-06 23:35:14
 * @LastEditors: wangjun haodreams@163.com
 * @LastEditTime: 2024-11-24 12:53:33
 * @FilePath: \golib\easyjs\iconv.go
 * @Description: 数据转换
 */
package easyjs

import (
	"gitee.com/haodreams/golib/textcode"
)

// 数据转换
type Iconv struct {
}

// GBK 转 UTF8 编码
func (m *Iconv) UTF8(text string) string {
	return textcode.UTF8(text)
}

// UTF8 转 GBK 编码
func (m *Iconv) GBK(text string) string {
	return textcode.GBK(text)
}
