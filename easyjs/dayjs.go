/*
 * @Author: wangjun haodreams@163.com
 * @Date: 2024-11-06 22:14:40
 * @LastEditors: wangjun haodreams@163.com
 * @LastEditTime: 2024-12-09 21:06:43
 * @FilePath: \ai_tradee:\go\src\gitee.com\haodreams\golib\easyjs\dayjs.go
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
package easyjs

import (
	"time"

	"gitee.com/haodreams/libs/easy"
	"github.com/dop251/goja"
)

//Weekday () 返回一周的第几天 = dayjs().day()

type Dayjs struct {
	ms int64
}

func (m *Dayjs) Name() string { return "Dayjs" }

func (m *Dayjs) Export() string {
	return "export function dayjs (ts)  {return null};\n"
}
func (m *Dayjs) Format(fmt string) string {
	if fmt == "" {
		return time.UnixMilli(m.ms).Format(easy.YYYYMMDDHHMMSS)
	}
	return time.UnixMilli(m.ms).Format(easy.ConvertFormat(fmt))
}

// YY	18	两位数的年份
// YYYY	2018	四位数的年份
// M	1-12	月份，从 1 开始
// MM	01-12	月份，两位数
// MMM	Jan-Dec	缩写的月份名称
// MMMM	January-December	完整的月份名称
// D	1-31	月份里的一天
// DD	01-31	月份里的一天，两位数
// H	0-23	小时
// HH	00-23	小时，两位数
// h	1-12	小时, 12 小时制
// hh	01-12	小时, 12 小时制, 两位数
// m	0-59	分钟
// mm	00-59	分钟，两位数
// s	0-59	秒
// ss	00-59	秒，两位数
// S	0-9	毫秒，一位数
// SS	00-99	毫秒，两位数
// SSS	000-999	毫秒，三位数
// Z	-05:00	UTC 的偏移量
// ZZ	-0500	UTC 的偏移量，两位数
// A	AM / PM	上午 下午 大写
// a	am / pm	上午 下午 小写
// Do	1st... 31st	带序数词的月份里的一天
// X	1410715640.579	Unix 时间戳
// x	1410715640579	Unix 时间戳

func NewDayjs(vm *goja.Runtime, arg any) *goja.Object {
	dayjs := vm.NewObject()

	dayjs.Set("format", func(fmt string) string {
		ms := dayjs.Get("ms").ToInteger()
		if fmt == "" {
			return time.UnixMilli(ms).Format(easy.YYYYMMDDHHMMSS)
		}
		return time.UnixMilli(ms).Format(easy.ConvertFormat(fmt))
	})

	//weekday = new Date().getDay()

	return dayjs
}

// 解析时间,返回毫秒
func ParseTime(arg any) int64 {
	if arg == nil {
		return time.Now().UnixMilli()
	}
	switch v := arg.(type) {
	case float64:
		if v < 1000000000000 {
			return int64(v * 1000)
		}
		return int64(v)
	case int64: //自动识别毫秒和秒级数据
		if v < 1000000000000 {
			return v * 1000
		}
		return v
	case int: //自动识别毫秒和秒级数据
		if v < 0x7fffffff {
			return int64(v) * 1000
		}
		return int64(v)
	case string:
		var t time.Time
		var err error
		switch len(v) {
		case 10:
			t, err = time.Parse(easy.YYYYMMDD, v)
		case 19:
			t, err = time.Parse(easy.YYYYMMDDHHMMSS, v)
		case 23:
			t, err = time.Parse(easy.YYYYMMDDHHMMSSMill, v)
		}
		if err == nil {
			return t.UnixMilli()
		}
	}
	return 0
}
