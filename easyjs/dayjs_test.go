package easyjs

import (
	"fmt"
	"testing"
	"time"

	"gitee.com/haodreams/golib/logs"
	"gitee.com/haodreams/libs/easy"
	"github.com/dop251/goja"
)

type Test struct {
}

func (m *Test) Print() {
	logs.Info("Testing")
}

func TestObject(t *testing.T) {
	fmt.Println(Exports(&Iconv{}))
	fmt.Println(Exports(&Dayjs{}))
	fmt.Println(Exports(&System{}))
}

type dj int64

func (dj) Name() string {
	return "dj"
}

func (m dj) Format(fmt string) string {
	if fmt == "" {
		return time.UnixMilli(int64(m)).Format(easy.YYYYMMDDHHMMSS)
	}
	return time.UnixMilli(int64(m)).Format(easy.ConvertFormat(fmt))
}

func TestDayjs(t *testing.T) {
	js := `let dayjs = this.dayjs2
	console.log(dayjs().Format("YYYY-MM-DD HH:mm:ss.SSS"))
	console.log(dayjs().Format("YYYY-MM-DD HH:mm:ss"))
	console.log(dayjs().Format("YYYY-MM-DD HH:mm:ss"))
	console.log(dayjs().Format("YYYY-MM-DD"))
	console.log(dayjs(1730857038451).Format("YYYY-MM-DD HH:mm:ss.SSS"))
	console.log(dayjs(1730857038.451).Format("YYYY-MM-DD HH:mm:ss.SSS"))
	console.log(dayjs(1730857038).Format("YYYY-MM-DD HH:mm:ss.SSS"))
	console.log(dayjs("2024-11-06 09:38:10").Format("YYYY-MM-DD HH:mm:ss.SSS"))
	console.log(dayjs("2024-11-06").Format("YYYY-MM-DD HH:mm:ss.SSS"))
	console.log(dayjs("2024-11-06 09:38:10.456").Format("YYYY-MM-DD HH:mm:ss.SSS"))
	test.Print()
	`
	vm := goja.New()
	Init(vm)
	vm.Set("dayjs2", func(arg any) dj {
		d := dj(ParseTime(arg))
		return d
	})
	vm.Set("dayjs", func(arg any) *Dayjs {
		dayjs := new(Dayjs)
		dayjs.ms = ParseTime(arg)
		return dayjs
	})
	Register(vm, &Test{})
	_, err := vm.RunString(js)
	if err != nil {
		logs.Warn(err)
		return
	}
}

func BenchmarkDayjs1(b *testing.B) {
	js := `	
	let dayjs = this.dayjs2
	function run(){
		dayjs().Format("YYYY-MM-DD HH:mm:ss.SSS")
		dayjs().Format("YYYY-MM-DD HH:mm:ss")
		dayjs().Format("YYYY-MM-DD HH:mm:ss")
		dayjs().Format("YYYY-MM-DD")
		dayjs(1730857038451).Format("YYYY-MM-DD HH:mm:ss.SSS")
		dayjs(1730857038.451).Format("YYYY-MM-DD HH:mm:ss.SSS")
		dayjs(1730857038).Format("YYYY-MM-DD HH:mm:ss.SSS")
		dayjs("2024-11-06 09:38:10").Format("YYYY-MM-DD HH:mm:ss.SSS")
		dayjs("2024-11-06").Format("YYYY-MM-DD HH:mm:ss.SSS")
		dayjs("2024-11-06 09:38:10.456").Format("YYYY-MM-DD HH:mm:ss.SSS")
	}
	`
	vm := goja.New()
	Init(vm)
	vm.Set("dayjs2", func(arg any) dj {
		d := dj(ParseTime(arg))
		return d
	})

	vm.Set("Test", &Test{})
	_, err := vm.RunString(js)
	if err != nil {
		logs.Warn(err)
		return
	}

	run, ok := goja.AssertFunction(vm.Get("run"))
	if !ok {
		logs.Error(err)
	}
	for n := 0; n < b.N; n++ {
		run(nil)
	}
}

func BenchmarkDayjs2(b *testing.B) {
	js := `	
	let dj = this.dayjs
	function run(){
		dj().Format("YYYY-MM-DD HH:mm:ss.SSS")
		dj().Format("YYYY-MM-DD HH:mm:ss")
		dj().Format("YYYY-MM-DD HH:mm:ss")
		dj().Format("YYYY-MM-DD")
		dj(1730857038451).Format("YYYY-MM-DD HH:mm:ss.SSS")
		dj(1730857038.451).Format("YYYY-MM-DD HH:mm:ss.SSS")
		dj(1730857038).Format("YYYY-MM-DD HH:mm:ss.SSS")
		dj("2024-11-06 09:38:10").Format("YYYY-MM-DD HH:mm:ss.SSS")
		dj("2024-11-06").Format("YYYY-MM-DD HH:mm:ss.SSS")
		dj("2024-11-06 09:38:10.456").Format("YYYY-MM-DD HH:mm:ss.SSS")	
	}
	`
	vm := goja.New()
	Init(vm)
	vm.Set("dayjs", func(arg any) *Dayjs {
		dayjs := new(Dayjs)
		dayjs.ms = ParseTime(arg)
		return dayjs
	})
	vm.Set("Test", &Test{})
	_, err := vm.RunString(js)
	if err != nil {
		logs.Warn(err)
		return
	}

	run, ok := goja.AssertFunction(vm.Get("run"))
	if !ok {
		logs.Error(err)
	}
	for n := 0; n < b.N; n++ {
		run(nil)
	}
}

func TestExec(t *testing.T) {
	js := `sys.Exec("ping","127.0.0.1")
	try{
	sys.ErrorData();
		console.log("OK")
	}catch(error){
		console.log(error)
	}
	console.log("END")
	`
	vm := goja.New()
	Init(vm)
	Register(vm, &System{})
	_, err := vm.RunString(js)
	if err != nil {
		logs.Warn(err)
		return
	}
}
