/*
 * @Author: Wangjun
 * @Date: 2021-08-16 09:07:58
 * @LastEditTime: 2024-11-14 20:25:07
 * @LastEditors: wangjun haodreams@163.com
 * @Description:
 * @FilePath: \golib\js\jstask\task.go
 * hnxr
 */
package taskjs

import (
	"os"
	"path/filepath"
	"time"

	"gitee.com/haodreams/golib/easyjs"
	"gitee.com/haodreams/golib/exception"
	"gitee.com/haodreams/golib/logs"
	"gitee.com/haodreams/libs/easy"
	"github.com/dop251/goja"
	"github.com/robfig/cron/v3"
)

type CallBack func(vm *goja.Runtime)

var cb CallBack

// GetCallBack 获取全局回调函数
func GetCallBack() CallBack {
	return cb
}

// 设置全局回调函数
func SetCallBack(f CallBack) {
	cb = f
}

// Task .
type Task struct {
	Time         string //执行时间
	Path         string //路径
	Desc         string //描述
	UsedTime     string //执行任务耗时
	LastCallTime int64  //最后一次调用的开始时间
	Msg          string
	Count        int
	Running      bool //执行状态
	vm           *goja.Runtime
	run          goja.Callable //主循环函数
	ID           int
	Timeout      int64 // 超时时间， 如果超过这个时间还没有执行完，则继续执行
	Initd        bool  //是否初始化过
}

// Tasks 任务组
type Tasks struct {
	Time  string
	ID    int
	tasks []*Task
}

// Cancel 取消任务
func (m *Task) Cancel() {
	m.vm = nil
}

// Run 运行任务
func (m *Tasks) Run() {
	for _, task := range m.tasks {
		if task != nil {
			task.Run()
		}
	}
}

// 和同步基本类似，区别是每次执行重新加载js
type CronTask struct {
	taskMap map[string]*Tasks
	array   []*Task
	cron    *cron.Cron
}

// GetTasks 获取全部的任务
func (m *CronTask) GetTasks() []*Task {
	return m.array
}

// SyncTask 同步
type SyncTask struct {
	taskMap map[string]*Tasks
	array   []*Task
	cron    *cron.Cron
}

// GetTasks 获取全部的任务
func (m *SyncTask) GetTasks() []*Task {
	return m.array
}

// AsyncTask 异步
type AsyncTask struct {
	Tasks
	cron *cron.Cron
}

// GetTasks 获取全部的任务
func (m *AsyncTask) GetTasks() []*Task {
	return m.tasks
}

func (m *Task) Run() {
	m.work()
	m.Count++
}

// Run 定时执行
func (m *Task) work() {
	now := time.Now()
	if m.Running {
		timeout := easy.IF(m.Timeout == 0, 3600, m.Timeout)
		if now.Unix()-timeout < m.LastCallTime {
			logs.Warn(m.Path, m.Desc, "服务尚未完成, 本次放弃执行")
			return
		}
	}
	m.LastCallTime = now.Unix()
	m.Running = true
	defer func() {
		exception.CatchException()
		m.Running = false
		m.UsedTime = time.Since(now).String()
	}()

	if !m.Initd {
		vm, err := LoadJs(m.Path)
		if err != nil {
			m.Msg = err.Error()
			return
		}
		vm.Set("SetRunTimeout", func(timeout int64) {
			m.Timeout = timeout //设置执行的最大超时时间
		})
		run, ok := goja.AssertFunction(vm.Get("run"))
		if !ok {
			m.Msg = "run() not a function"
			logs.Warn(m.Msg)
			return
		}
		m.run = run
		m.vm = vm
	}

	if m.vm != nil && m.run != nil {
		data, err := m.run(nil)
		if err != nil {
			logs.Warn(m.Path, m.Desc, err)
			m.Msg = err.Error()
			return
		}
		if data == goja.Undefined() {
			return
		}
		m.Msg = data.String()
	}
}

func LoadJs(path string) (vm *goja.Runtime, err error) {
	dada, err := os.ReadFile(path)
	if err != nil {
		logs.Warn(err)
		return
	}
	vm = goja.New()
	if cb != nil {
		cb(vm)
	}

	name := filepath.Base(path)
	_, err = easyjs.RunScript(vm, name, string(dada))
	if err != nil {
		logs.Warn(err)
		return
	}
	return
}
