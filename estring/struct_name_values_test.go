/*
 * @Author: Wangjun
 * @Date: 2021-05-15 22:43:19
 * @LastEditTime: 2022-08-08 11:17:10
 * @LastEditors: Wangjun
 * @Description:
 * @FilePath: \golib\estring\struct_name_values_test.go
 * hnxr
 */
package estring

import (
	"log"
	"strings"
	"testing"
)

type Dev struct {
	ID         int
	ModeID     int
	CreateTime string
	V1         float64
	V2         *float64
}

func TestA(t *testing.T) {
	log.Println(strings.EqualFold("aB/", "Ab/"))
}

func TestTitle(t *testing.T) {
	dev := new(Dev)
	dev.CreateTime = "test"
	dev.V1 = 100
	titles := GetTitlesFromStruct(dev)
	log.Println(titles)

	log.Println(GetNameStruct(dev, true))
}

func TestValue(t *testing.T) {
	dev := new(Dev)
	dev.CreateTime = "test"
	dev.V1 = 100
	vals := GetStructValues(dev)
	log.Println(vals)
	dev.V2 = &dev.V1
	vals = GetStructValues(dev)
	log.Println(vals)

}
