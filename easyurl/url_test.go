/*
 * @Author: wangjun haodreams@163.com
 * @Date: 2024-12-28 17:13:01
 * @LastEditors: wangjun haodreams@163.com
 * @LastEditTime: 2025-03-06 14:27:21
 * @FilePath: \golib\easyurl\url_test.go
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
package easyurl

import (
	"log"
	"testing"
)

func TestJJQuotaion(t *testing.T) {
	os := NewOptions(WithDecodeGBK())
	url := "https://127.0.0.1:8082"

	data, code, err := Get(url, os)
	if err != nil {
		log.Fatal(err)
	}
	log.Println(code)
	log.Println(string(data))
}

func TestURL(t *testing.T) {
	data, code, err := GET("https://e051d817096a.addr.dev.com:8080/", WithProxy("http://124.221.142.115:28081"))
	// log.Println(err)
	// log.Println(code)
	// log.Println(string(data))
}
