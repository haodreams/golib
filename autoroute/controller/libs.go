/*
 * @Author: Wangjun
 * @Date: 2021-05-15 22:43:19
 * @LastEditTime: 2021-11-25 10:10:14
 * @LastEditors: Wangjun
 * @Description:
 * @FilePath: \xrcalcd:\go\src\gitee.com\haodreams\golib\autoroute\controller\libs.go
 * hnxr
 */
package controller

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

//应答的数据结构
type RsponseBody struct {
	Code int         `json:"code"`
	Msg  string      `json:"msg"`
	Data interface{} `json:"data"`
}

//Msg ...
func Msg(c *gin.Context, msg string) {
	c.JSON(http.StatusOK, gin.H{
		"code": http.StatusOK,
		"msg":  msg,
	})
}

//Error ...
func Error(c *gin.Context, msg string) {
	//405 客户端请求中的方法被禁止
	c.JSON(http.StatusInternalServerError, gin.H{
		"code": http.StatusInternalServerError,
		"msg":  msg,
	})
}

//ErrorWithCode ...
func ErrorWithCode(c *gin.Context, msg string, code int) {
	c.JSON(code, gin.H{
		"code": code,
		"msg":  msg,
	})
}

//IsPost 是否是post方法提交
func IsPost(c *gin.Context) bool {
	return c.Request.Method == "POST"
}
