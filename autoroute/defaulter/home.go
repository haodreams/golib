package defaulter

import (
	"gitee.com/haodreams/golib/autoroute/controller"
)

//HomeController 首页相关的操作
type HomeController struct {
	controller.Controller
}

//Index 获取默认主页
func (m *HomeController) Index() {
	m.Display()
}
