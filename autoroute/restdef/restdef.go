/*
 * @Author: Wangjun
 * @Date: 2021-11-01 10:59:05
 * @LastEditTime: 2021-11-01 11:06:03
 * @LastEditors: Wangjun
 * @Description:
 * @FilePath: \xr_ai_monitord:\go\src\gitee.com\haodreams\golib\autoroute\restdef\restdef.go
 * hnxr
 */
package restdef

/**
 * @description: 接口的应答消息
 * @param {*}
 * @return {*}
 */
type RestAck struct {
	Data  interface{} `json:"data"`
	Code  int         `json:"code"`
	Msg   string      `jisn:"msg"`
	Count int         `json:"count"`
}
