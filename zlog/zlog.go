/*
 * @Author: Wangjun
 * @Date: 2021-07-20 10:22:48
 * @LastEditTime: 2021-08-03 11:33:31
 * @LastEditors: Wangjun
 * @Description:日志
 * @FilePath: \scadae:\go\src\gitee.com\haodreams\golib\zlog\zlog.go
 * hnxr
 */
package zlog

import (
	"io"
	"os"

	"github.com/rs/zerolog"
)

var Log *zerolog.Logger

//zerolog.TimeFieldFormat = "2006-01-02 15:04:05.000"
//log.Logger = zerolog.New(multi).With().Timestamp().Logger()

var file *os.File

/**
 * @description: 初始化数据
 * @param {string} path
 * @param {string} options
 * @return {*}
 */
func Init(path string, options string, otherWriter io.Writer) (err error) {
	file, err = os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_SYNC, 0666)
	if err != nil {
		return
	}
	if otherWriter == nil {
		l := zerolog.New(file).With().Timestamp().Logger()
		Log = &l
	} else {
		write := zerolog.MultiLevelWriter(file, otherWriter)
		l := zerolog.New(write).With().Timestamp().Logger()
		Log = &l
	}

	return
}

/**
 * @description: 释放资源
 * @param {*}
 * @return {*}
 */
func Close() {
	if file != nil {
		file.Close()
		file = nil
	}
}
