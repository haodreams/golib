package wrap

import (
	"log"
	"testing"
)

// TestOKWarp 正确用法
func TestOKWarp(t *testing.T) {
	wg := NewWaitGroupWrapper(0)
	for i := 0; i < 100; i++ {
		ii := i
		wg.Wrap(func() {
			log.Println(ii)
		})
	}
	wg.Wait()
}

// TestErrorWarp 错误用法
func TestErrorWarp(t *testing.T) {
	wg := NewWaitGroupWrapper(0)
	for i := 0; i < 100; i++ {
		wg.Wrap(func() {
			log.Println(i)
		})
	}
	wg.Wait()
}
