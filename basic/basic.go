/*
 * @Author: Wangjun
 * @Date: 2021-11-17 14:25:27
 * @LastEditTime: 2021-11-17 14:35:58
 * @LastEditors: Wangjun
 * @Description:
 * @FilePath: \xr_ai_monitord:\go\src\gitee.com\haodreams\golib\basic\basic.go
 * hnxr
 */
package basic

import "time"

type Object struct {
	ObjectName string //对象名
	CreateTime int64  //创建时间
	Enable     bool   //是否可用
	Error      error  //是否有错误
}

/**
 * @description:
 * @param {*}
 * @return {*}
 */
func (m *Object) IsEnable() bool {
	return m.Enable
}

/**
 * @description:
 * @param {*}
 * @return {*}
 */
func (m *Object) SetOK(now ...int64) {
	m.Enable = true
	if len(now) > 0 && now[0] != 0 {
		m.CreateTime = now[0]
	} else {
		m.CreateTime = time.Now().Unix()
	}
}
