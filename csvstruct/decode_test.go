/*
 * @Author: Wangjun
 * @Date: 2022-10-20 14:01:16
 * @LastEditTime: 2022-10-20 15:29:31
 * @LastEditors: Wangjun
 * @Description:
 * @FilePath: \golib\csvstruct\decode_test.go
 * hnxr
 */
package csvstruct

import (
	"bytes"
	"io"
	"log"
	"testing"
)

type TC struct {
	Age int
}

type Test struct {
	ID   int
	Name string
	TC
}

var data = `ID,name,age
1,a1,100
2,b1,10
3,b3,96`

func TestDecodeAll(t *testing.T) {
	var k []*Test
	err := Decode(bytes.NewBufferString(data), &k)
	if err != nil {
		log.Println(err)
		return
	}
	for _, b := range k {
		log.Printf("%+v", *b)
	}
}

func TestDecode(t *testing.T) {
	var k []*Test
	dec := NewDecoder(bytes.NewBufferString(data))

	for {
		tt := new(Test)
		err := dec.DecodeNext(tt)
		if err == io.EOF {
			break
		}
		k = append(k, tt)
	}
	for _, b := range k {
		log.Printf("%+v", *b)
	}
}

func TestDecodeMap(t *testing.T) {
	var k []map[string]string
	dec := NewDecoder(bytes.NewBufferString(data))

	for {
		tt := make(map[string]string)
		err := dec.DecodeNext(tt)
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Println(err)
			break
		}
		k = append(k, tt)
	}
	for _, b := range k {
		log.Printf("%+v", b)
	}
}

func TestDecodeMapPtr(t *testing.T) {
	var k []map[string]string
	dec := NewDecoder(bytes.NewBufferString(data))

	for {
		tt := make(map[string]string)
		err := dec.DecodeNext(&tt)
		if err == io.EOF {
			break
		}
		k = append(k, tt)
	}
	for _, b := range k {
		log.Printf("%+v", b)
	}
}
