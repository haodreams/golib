/*
 * @Author: Wangjun
 * @Date: 2023-01-06 10:30:04
 * @LastEditTime: 2023-01-06 10:31:20
 * @LastEditors: Wangjun
 * @Description:
 * @FilePath: \golib\startruntime\start_run_time.go
 * hnxr
 */
package startruntime

import "time"

var startRunTime = time.Now().UnixMilli()

/**
 * @description: 获取程序的开始运行时间,单位秒
 * @return {*}
 */
func GetStartRunTime() int64 {
	return startRunTime / 1000
}

/**
 * @description: 获取程序的开始运行时间,单位毫秒
 * @return {*}
 */
func GetStartRunTimeMs() int64 {
	return startRunTime
}
