/*
 * @Author: Wangjun
 * @Date: 2022-08-19 16:19:26
 * @LastEditTime: 2022-08-19 16:20:03
 * @LastEditors: Wangjun
 * @Description:
 * @FilePath: \golib\gdb\sqlite3.go
 * hnxr
 */
package gdb

import (
	"gitee.com/haodreams/godriver/sqlite3"
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
)

func GetSqlite3(dsn string) (db *gorm.DB, err error) {
	return gorm.Open(sqlite3.Open(dsn), &gorm.Config{
		NamingStrategy: schema.NamingStrategy{
			SingularTable: true,
		},
	})
}
