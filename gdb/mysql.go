/*
 * @Author: Wangjun
 * @Date: 2022-08-19 16:20:35
 * @LastEditTime: 2022-08-19 16:20:58
 * @LastEditors: Wangjun
 * @Description:
 * @FilePath: \golib\gdb\mysql.go
 * hnxr
 */
package gdb

import (
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
)

func GetMysql(dsn string) (db *gorm.DB, err error) {
	return gorm.Open(mysql.Open(dsn), &gorm.Config{
		NamingStrategy: schema.NamingStrategy{
			SingularTable: true,
		},
	})
}
