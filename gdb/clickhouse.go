/*
 * @Author: Wangjun
 * @Date: 2022-08-19 16:17:31
 * @LastEditTime: 2023-06-02 10:22:39
 * @LastEditors: Wangjun
 * @Description:
 * @FilePath: \golib\gdb\clickhouse.go
 * hnxr
 */
package gdb

import (
	"gitee.com/haodreams/godriver/clickhouse"
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
)

// 获取clickhouse数据库驱动
func GetClickhouse(dsn string) (db *gorm.DB, err error) {
	return gorm.Open(clickhouse.Open(dsn), &gorm.Config{
		NamingStrategy: schema.NamingStrategy{
			SingularTable: true,
		},
	})
}
