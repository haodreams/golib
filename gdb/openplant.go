/*
 * @Author: Wangjun
 * @Date: 2022-08-19 16:21:45
 * @LastEditTime: 2022-08-19 16:35:42
 * @LastEditors: Wangjun
 * @Description:
 * @FilePath: \golib\gdb\openplant.go
 * hnxr
 */
package gdb

import (
	"gitee.com/haodreams/godriver/opdb/opsql"
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
)

func GetOpenplant(dsn string) (db *gorm.DB, err error) {
	return gorm.Open(opsql.Open(dsn), &gorm.Config{
		NamingStrategy: schema.NamingStrategy{
			SingularTable: true,
		},
	})
}
