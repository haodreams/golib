package gdb

import (
	"errors"
	"time"

	"gitee.com/haodreams/golib/logs"
	"gitee.com/haodreams/libs/ee"
	"gorm.io/gorm"
)

// LoadDB 加载的DB
func LoadDB(dbType, dsn string) (db *gorm.DB, err error) {
	switch dbType {
	case "sqlite3":
		db, err = GetSqlite3(dsn)
	case "mysql":
		db, err = GetMysql(dsn)
	case "clickhouse":
		db, err = GetClickhouse(dsn)
	case "opsql":
		db, err = GetOpenplant(dsn)
	default:
		err = ee.Print(errors.New("不支持的驱动"+dbType), logs.CbWarn)
	}
	if err != nil {
		err = ee.Print(err, logs.CbWarn)
		return
	}
	gdb, err := db.DB()
	if err != nil {
		return
	}
	gdb.SetConnMaxLifetime(time.Second * 1800)
	return
}
