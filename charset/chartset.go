/*
 * @Author: wangjun haodreams@163.com
 * @Date: 2024-10-20 16:14:49
 * @LastEditors: wangjun haodreams@163.com
 * @LastEditTime: 2024-12-16 17:23:13
 * @FilePath: \golib\charset\chartset.go
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
package charset

import (
	"io"

	"golang.org/x/text/encoding/simplifiedchinese"
)

/**
 * @description: gbk 转为utf8编码
 * @param {string} data
 * @return {*}
 */
func UTF8(data string) string {
	ret, _ := simplifiedchinese.GBK.NewDecoder().String(data)
	return ret
}

/**
 * @description: gbk 转为utf8编码
 * @param {string} data
 * @return {*}
 */
func GBK(data string) string {
	ret, _ := simplifiedchinese.GBK.NewEncoder().String(data)
	return ret
}

// 读取GBK字节流
func GBKReader(r io.Reader) io.Reader {
	return simplifiedchinese.GBK.NewDecoder().Reader(r)
}

// 写入GBK字节流
func GBKWriter(w io.Writer) io.Writer {
	return simplifiedchinese.GBK.NewEncoder().Writer(w)
}
