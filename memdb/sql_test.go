/*
 * @Author: wangjun haodreams@163.com
 * @Date: 2024-05-08 18:40:50
 * @LastEditors: wangjun haodreams@163.com
 * @LastEditTime: 2024-05-17 09:47:22
 * @FilePath: \golib\memsql\sql_test.go
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
package memdb

import (
	"log"
	"testing"

	"gitee.com/haodreams/libs/memtable"
)

type BasicFan struct {
	ID           int     `json:"id"`           // id
	CompanyID    int     `json:"companyId"`    // 公司id
	CompanyName  string  `json:"companyName"`  // 公司名称
	PlantID      int     `json:"plantId"`      // 场站id
	PlantName    string  `json:"plantName"`    // 场站名称
	StagingID    int     `json:"stagingId"`    // 工期id
	StagingName  string  `json:"stagingName"`  // 工期名称
	CircuitID    int     `json:"circuitId"`    // 集电线id
	CircuitName  string  `json:"circuitName"`  // 集电线名称
	FanName      string  `json:"fanName"`      // 风机名称
	PowerField   string  `json:"powerField"`   //电量计算的原始点
	FanCode      string  `json:"fanCode"`      // 风机编码
	InnerCode    string  `json:"innerCode"`    // 内部编码
	ModelID      int     `json:"modelId"`      // 型号id
	ModelName    string  `json:"modelName"`    // 型号名称
	Status       int     `json:"status"`       // 1 运行 2 调试 3 未接入
	StartSpeed   float64 `json:"startSpeed"`   // 切入风速(m/s)
	StopSpeed    float64 `json:"stopSpeed"`    // 切出风速(m/s)
	FanCap       float64 `json:"fanCap"`       // 装机容量
	IsParadigm   int     `json:"isParadigm"`   //是否是标杆
	FanLocalType string  `json:"fanLocalType"` //fan_local_type 海风陆风
	CL           float64
	DP           float64
}

func TestQuery(t *testing.T) {
	var fs []*BasicFan
	f := new(BasicFan)
	f.ID = 1
	f.PlantID = 100
	f.FanCap = 2300
	f.DP = 1
	f.CircuitID = 100
	fs = append(fs, f)
	f = new(BasicFan)
	f.ID = 2
	f.PlantID = 100
	f.FanCap = 2200
	f.CircuitID = 100
	f.DP = 5
	fs = append(fs, f)
	f = new(BasicFan)
	f.ID = 3
	f.PlantID = 301
	f.FanCap = 2500
	f.CircuitID = 100
	f.DP = 2
	fs = append(fs, f)
	tab, err := memtable.NewTable(fs, "ID")
	if err != nil {
		log.Println(err)
		return
	}
	db := NewDatabase()
	db.RegisterMemTable(tab, "fan")

	//s := "SELECT CompanyName,CircuitName,StagingName,PlantID,PlantName,CircuitID,StagingID, DP, Count(*) AS cnt, Sum(EK) as ek  FROM inverter where  DP = 4 group by StagingID,DP"
	// group := new(sync.WaitGroup)
	// group.Add(10000)
	// for i := 0; i < 10000; i++ {
	// 	go func() {
	// 		s := fmt.Sprintf("SELECT CompanyName,CircuitName,StagingName,PlantID,PlantName,CircuitID,StagingID, DP, Count(*) AS cnt, Sum(EK) as ek  FROM inverter where  DP = %d group by StagingID,DP", i)
	// 		_, err := db.sqlparse.ParseString("", s)
	// 		if err != nil {
	// 			log.Fatal(err)
	// 		}
	// 		group.Done()
	// 	}()
	// }
	// group.Wait()

	//table, err := db.Query("SELECT CircuitID,DP, Count(*) AS cnt FROM fan WHERE ID=1 GROUP BY CircuitID,DP FILL BY CircuitID,DP(1,2,3,4,5,6)") //
	err = tab.UpdateCell(3, "DP", 1)
	if err != nil {
		log.Println(err)
		return
	}
	table, err := db.Query("SELECT ID,CircuitID,DP, Count(*) AS cnt FROM fan WHERE ID<PlantID/100") //
	//table, err := db.Query("SELECT CircuitID, Count(*) AS cnt FROM fan WHERE ID=1 GROUP BY CircuitID")
	//table, err := db.Query("SELECT CircuitID,StagingID,FanCap FROM fan group by CircuitID,StagingID order by FanCap")
	//table, err := db.Query("SELECT CircuitID,StagingID,FanCap FROM fan group by CircuitID,StagingID order by FanCap DESC")
	//table, err := db.Query("select Count(*) as cnt from fan where CL > 5 ")
	//table, err := db.Query("select Count(*) as cnt from fan ")
	//table, err := db.Query("select PlantID as pid, Max(FanCap) as maxc, Avg(FanCap) as avgc from BasicFan where ID > 0  group by PlantID")

	if err != nil {
		log.Println(err)
		return
	}
	table.Save("stdout")
}

func TestSQL(t *testing.T) {
	// sel, err := parser.ParseString("", `select PlantID as pid, Max(FanCap) as maxc, Avg(FanCap) as avgc from BasicFan where ID > 0 and a is null and b like 'ccc%' GROUP BY PlantID,CPYID`)
	// log.Println(err)
	// log.Println(sel)
}
