/*
 * @Date: 2022-09-25 22:29:03
 * @LastEditors: Wangjun
 * @LastEditTime: 2022-11-12 16:37:32
 * @FilePath: \golib\ttf\ttf.go
 * @Description:
 */
package ttf

// GetDefaultTTF 获取默认的ttf
func GetDefaultTTF() []byte {
	return ttfDroidSansFallback
}

// GetDroidSansFallback 。
func GetDroidSansFallback() []byte {
	return ttfDroidSansFallback
}
