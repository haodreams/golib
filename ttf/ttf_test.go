/*
 * @Date: 2022-09-25 22:14:31
 * @LastEditors: Wangjun
 * @LastEditTime: 2022-09-25 23:00:47
 * @FilePath: \golib\ttf\ttf_test.go
 * @Description:
 */
package ttf

import (
	"bytes"
	"fmt"
	"os"
	"testing"

	"github.com/pierrec/lz4"
)

func encode(b []byte) (data []byte, err error) {
	buf := bytes.NewBuffer(nil)
	zw := lz4.NewWriter(buf)
	zw.BlockMaxSize = 1024 * 1024
	zw.CompressionLevel = 9

	_, err = zw.Write(b)
	if err != nil {
		return
	}
	zw.Close()
	data = buf.Bytes()
	return
}

func TestXxx(t *testing.T) {
	data, err := os.ReadFile("DroidSansFallback.ttf")
	if err != nil {
		panic(err)
	}
	data, err = encode(data)
	if err != nil {
		panic(err)
	}

	f, err := os.Create("ttf_bin_droidsansfallback.go")
	if err != nil {
		panic(err)
	}

	defer f.Close()
	f.WriteString("package ttf\n")
	f.WriteString("var ttfBinDroidSansFallback = []byte{")
	for i, c := range data {
		if i%100 == 0 && i != 0 {
			f.WriteString(fmt.Sprintf("%d,\n", c))
		} else {
			f.WriteString(fmt.Sprintf("%d,", c))
		}
	}
	f.WriteString("\n}")

}
