/*
 * @Author: wangjun haodreams@163.com
 * @Date: 2024-11-06 23:35:14
 * @LastEditors: wangjun haodreams@163.com
 * @LastEditTime: 2024-11-24 12:52:26
 * @FilePath: \golib\easyjs\iconv.go
 * @Description: 数据转换
 */
package textcode

import "golang.org/x/text/encoding/simplifiedchinese"

// GBK 转 UTF8 编码
func UTF8(text string) string {
	ret, _ := simplifiedchinese.GBK.NewDecoder().String(text)
	return ret
}

//UTF8 转 GBK 编码
func GBK(text string) string {
	ret, _ := simplifiedchinese.GBK.NewEncoder().String(text)
	return ret
}
