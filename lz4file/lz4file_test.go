package lz4file

import (
	"log"
	"testing"
)

func TestEncode(t *testing.T) {
	input1 := []byte("Hello, World!")
	encoded, err := Encode(input1)
	if err != nil {
		log.Printf("Encode failed: %v", err)
	}
	log.Printf("Encoded: %02x", encoded)

	decoded, err := Decode(encoded)
	if err != nil {
		log.Printf("Decode failed: %v", err)
	}
	log.Printf("Decoded: %s", decoded)
}
