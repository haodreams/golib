/*
 * @Author: Wangjun
 * @Date: 2022-11-14 16:04:20
 * @LastEditTime: 2022-11-14 16:07:33
 * @LastEditors: Wangjun
 * @Description:
 * @FilePath: \golib\memory\test\main.go
 * hnxr
 */
package main

import (
	"log"
	"os"
	"time"

	"gitee.com/haodreams/libs/easy"
	"gitee.com/haodreams/golib/memory"
)

func main() {

	var mem memory.Memory
	log.Println(easy.BeautifySize(int64(mem.GetMemoryStats(time.Now().Unix(), os.Getpid()).Alloc)))
}
