package main

import (
	"fmt"
	"log"
	"net"
	"os"
	"path/filepath"
	"time"

	"gitee.com/haodreams/golib/env"
	"gitee.com/haodreams/golib/wall"
	"gitee.com/haodreams/libs/easy"
)

func main() {
	env.Setup("app.conf")
	appName := filepath.Base(os.Args[0])
	fmt.Printf("服务端模式: %s [server]\n", appName)
	fmt.Printf("客户端模式: %s client \n", appName)
	if len(os.Args) > 1 && os.Args[1] == "client" {
		send := wall.NewSend()
		for {
			err := send.Connected()
			if err != nil {
				log.Println(err)
				return
			}
			msg := easy.FormatNow(easy.YYYYMMDDHHmmssSSS)
			_, err = send.Write([]byte(msg))
			if err != nil {
				log.Println(err)
				return
			}
			time.Sleep(time.Second)
			log.Println("Send:", msg)
		}
	}

	recv := wall.NewRecv()
	listen, err := recv.Listen()
	if err != nil {
		log.Fatalln(err)
	}
	log.Println("work on :", listen.Addr())
	for {
		conn, err := listen.AcceptTCP()
		if err != nil {
			log.Fatalln(err)
		}
		go func(conn *net.TCPConn) {
			defer conn.Close()
			for {
				data, err := recv.Read(conn)
				if err != nil {
					log.Println(err)
					return
				}
				log.Println("Recv:", string(data))
			}
		}(conn)
	}
}
