package wall

import (
	"encoding/binary"
	"net"
	"time"

	"gitee.com/haodreams/libs/sock"

	"gitee.com/haodreams/libs/config"
)

// Recv .
type Recv struct {
	ackChar       byte //确认字符
	listenAddress string
	timeout       time.Duration
}

// NewRecv .
func NewRecv() *Recv {
	m := new(Recv)
	m.listenAddress = config.DefaultString("listen.addr", ":13000")
	m.ackChar = byte(config.DefaultInt("ack.char", 0))
	m.timeout = time.Duration(config.DefaultInt("timeout", 30)) * time.Second

	return m
}

func (m *Recv) Listen() (conn *net.TCPListener, err error) {
	return sock.CreateTcpListener(m.listenAddress)
}

// Read .
func (m *Recv) Read(conn *net.TCPConn) (data []byte, err error) {
	data = make([]byte, 2)
	conn.SetReadDeadline(time.Now().Add(m.timeout))
	_, err = sock.Read(conn, data)
	if err != nil {
		return
	}
	l := binary.BigEndian.Uint16(data)
	isLz4Frame := l&MaskLz4 == MaskLz4
	l &= MaskNone
	data = make([]byte, l)
	_, err = sock.Read(conn, data)
	if err != nil {
		return
	}

	if isLz4Frame {
		data, err = Lz4Decode(data)
		if err != nil {
			return
		}
	}

	_, err = conn.Write([]byte{m.ackChar})
	return
}
