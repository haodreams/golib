package jsutil

import (
	"os"

	"gitee.com/haodreams/libs/easy"
)

/**
 * @description: 删除文件
 * @param {string} path
 * @return {*}
 */
func (m *JsThis) Remove(path string) any {
	_, err := os.Stat(path)
	if err != nil {
		return nil
	}
	err = os.Remove(path)
	if err != nil {
		return err
	}
	return nil
}

/**
 * @description: 文件或目录是否存在
 * @param {string} path
 * @return {*}
 */
func (m *JsThis) IsExist(path string) bool {
	_, err := os.Stat(path)
	return err == nil
}

/**
 * @description: 是否是一个文件
 * @param {string} path
 * @return {*}
 */
func (m *JsThis) IsFile(path string) bool {
	f, err := os.Stat(path)
	if err != nil {
		return false
	}
	if f.IsDir() {
		return false
	}
	return true
}

/**
 * @description: 新建一个目录
 * @param {string} path
 * @return {*}
 */
func (m *JsThis) Mkdir(path string) bool {
	err := easy.MkdirAll(path)
	return err != nil
}

/**
 * @description: 获取文件的更新时间
 * @param {string} path
 * @return {*}
 */
func (m *JsThis) FileModiTime(path string) int64 {
	fi, err := os.Stat(path)
	if err != nil {
		return -1
	}
	return fi.ModTime().Unix()
}

/**
 * @description: 是否是一个目录
 * @param {string} path
 * @return {*}
 */
func (m *JsThis) IsDir(path string) bool {
	f, err := os.Stat(path)
	if err != nil {
		return false
	}
	if f.IsDir() {
		return true
	}
	return false
}

/**
 * @description: 追加的方式文件
 * @param {*} path
 * @param {string} data
 * @return {*}
 */
func (m *JsThis) AppendFile(path, data string) any {
	f, err := os.OpenFile(path, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		return err
	}
	defer f.Close()
	_, err = f.WriteString(data)
	return err
}

/**
 * @description: 写文件
 * @param {*} path
 * @param {string} data
 * @return {*}
 */
func (m *JsThis) WriteFile(path, data string) any {
	return os.WriteFile(path, []byte(data), 0644)
}

/**
 * @description: 读取文件的内容
 * @param {string} path
 * @return {*}
 */
func (m *JsThis) ReadFile(path string) any {
	data, err := os.ReadFile(path)
	if err != nil {
		return err
	}
	return string(data)
}

/**
 * @description: 读取文件的内容
 * @param {string} path
 * @return {*}
 */
func (m *JsThis) RemoveFile(path string) any {
	_, err := os.Stat(path)
	if err != nil {
		return nil
	}
	err = os.Remove(path)
	if err != nil {
		return err
	}
	return nil
}
