/*
 * @Author: Wangjun
 * @Date: 2023-03-15 13:37:10
 * @LastEditTime: 2023-06-29 10:09:14
 * @LastEditors: Wangjun
 * @Description:
 * @FilePath: \golib\js\jsutil\task_test.go
 * hnxr
 */
package jsutil

import (
	"log"
	"testing"
	"time"

	"gitee.com/haodreams/libs/easy"
	"github.com/dop251/goja"
)

func TestLog(t *testing.T) {
	const SCRIPT = `
	function log(x){
		console.trace(x)
	}
    var i = 0;
	log("test")
	//console.trace(i)
    `

	vm := goja.New()
	InitModels(vm)
	time.AfterFunc(1000*time.Millisecond, func() {
		vm.Interrupt("halt")
	})

	rv, err := vm.RunString(SCRIPT)
	if err == nil {
		t.Fatal("Err is nil")
	}
	t.Log(rv)
}

func TestFmt(t *testing.T) {
	fmt := "YYYY-MM-DD HH:mm:ss.SSS"
	log.Println(easy.ConvertFormat(fmt))
}

func TestInterrupt(t *testing.T) {
	const SCRIPT = `
    var i = 0;
    for (;;) {
        i++;
    }
    `

	vm := goja.New()
	time.AfterFunc(1000*time.Millisecond, func() {
		vm.Interrupt("halt")
	})

	rv, err := vm.RunString(SCRIPT)
	if err == nil {
		t.Fatal("Err is nil")
	}
	t.Log(rv, err)
	// err is of type *InterruptError and its Value() method returns whatever has been passed to vm.Interrupt()
}
