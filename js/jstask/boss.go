/*
 * @Date: 2021-08-15 23:11:38
 * @LastEditors: wangjun haodreams@163.com
 * @LastEditTime: 2024-11-01 09:27:25
 * @FilePath: \golib\js\jstask\boss.go
 * @Description:
 */
package jstask

var boss *Boss

type Jober interface {
	GetTasks() []*Task
}

type Boss struct {
	Job Jober
	// CronTask  *CronTask
	// SyncTask  *SyncTask
	// AsyncTask *AsyncTask
}

// GetLuadbBoss 获取管理boss
func GetTasks() *Boss {
	return boss
}
