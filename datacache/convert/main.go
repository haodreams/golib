package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"

	"gitee.com/haodreams/golib/datacache"
)

func main() {
	if len(os.Args) < 2 {
		fmt.Println(os.Args[0], "filename")
		return
	}

	f, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	buf := bufio.NewReader(f)
	data := make([]byte, 17)
	var item datacache.Item
	for {
		n, err := io.ReadFull(buf, data)
		if n != 17 || err != nil {
			//log.Fatal(err)
			return
		}
		er := item.Decode(data)
		if er != nil {
			log.Fatal(er)
		}
		fmt.Println(item.String())
	}
}
