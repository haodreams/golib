/*
 * @Author: Wangjun
 * @Date: 2021-05-15 22:43:19
 * @LastEditTime: 2022-04-11 09:22:49
 * @LastEditors: Wangjun
 * @Description:
 * @FilePath: \golib\dbutils\dbutils.go
 * hnxr
 */
package dbutils

import "gorm.io/gorm"

// Save 保存数据， 有则更新，无则插入
func Save(db *gorm.DB, record interface{}) (err error) {
	newdb := db.Model(record).Updates(record)
	if newdb.Error == nil && newdb.RowsAffected == 0 {
		return db.Create(record).Error
	}
	return newdb.Error
}
