package sql

import (
	"fmt"
	"log"
	"testing"
)

func TestSqlSelect(t *testing.T) {
	s := "select * from time"
	varCmd, err := ParserSQL(s)
	if err != nil {
		log.Println("\nSQL:", s, err.Error())
	} else {
		switch cmd := varCmd.(type) {
		case *SelectCommand:
			log.Println("\nSQL :"+cmd.SQL+"\nSELECT", cmd.Table, cmd.Fields, cmd.Where, cmd.Limit)
		}
	}
	fmt.Println()

	s = "select  from time"
	varCmd, err = ParserSQL(s)
	if err != nil {
		log.Println("\nSQL:", s, err.Error())
	} else {
		switch cmd := varCmd.(type) {
		case *SelectCommand:
			log.Println("\nSQL :"+cmd.SQL+"\nSELECT", cmd.Table, cmd.Fields, cmd.Where, cmd.Limit)
		}
	}
	fmt.Println()

	s = "select * from time where name='aaa'"
	varCmd, err = ParserSQL(s)
	if err != nil {
		log.Println("\nSQL:", s, err.Error())
	} else {
		switch cmd := varCmd.(type) {
		case *SelectCommand:
			log.Println("\nSQL :"+cmd.SQL+"\nSELECT", cmd.Table, cmd.Fields, cmd.Where, cmd.Limit)
		}
	}
	fmt.Println()

	s = "select * from time where limit 20"
	varCmd, err = ParserSQL(s)
	if err != nil {
		log.Println("\nSQL:", s, err.Error())
	} else {
		switch cmd := varCmd.(type) {
		case *SelectCommand:
			log.Println("\nSQL :"+cmd.SQL+"\nSELECT", cmd.Table, cmd.Fields, cmd.Where, cmd.Limit)
		}
	}
	fmt.Println()

	s = "select * from time where limit 200,20"
	varCmd, err = ParserSQL(s)
	if err != nil {
		log.Println("\nSQL:", s, err.Error())
	} else {
		switch cmd := varCmd.(type) {
		case *SelectCommand:
			log.Println("\nSQL :"+cmd.SQL+"\nSELECT", cmd.Table, cmd.Fields, cmd.Where, cmd.Limit)
		}
	}

}
