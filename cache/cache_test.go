package cache

import (
	"fmt"
	"log"
	"testing"

	"gitee.com/haodreams/golib/logs"
)

// 测试缓存读取
func TestSaveCache(t *testing.T) {
	cache := new(Cache)
	cache.Init()
	for i := 0; i < 100; i++ {
		cache.Set(fmt.Sprint(i), fmt.Sprint(i*100))
	}
	cache.Save("test.lz4.dat")
}

// 测试缓存加载
func TestLoadCache(t *testing.T) {
	cache := new(Cache)
	cache.Init()
	err := cache.Load("test.lz4.dat")
	if err != nil {
		logs.Error(err.Error())
		return
	}

	cache.Each(func(key, value string) error {
		log.Println(key, value)
		return nil
	})
}
