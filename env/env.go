/*
 * @Date: 2021-06-30 20:36:39
 * @LastEditors: wangjun haodreams@163.com
 * @LastEditTime: 2024-12-18 13:23:02
 * @FilePath: \golib\env\env.go
 * @Description:
 */
package env

//通用环境配置

import (
	"flag"
	"goversion"
	"log"
	"net"
	"os"
	"path/filepath"
	"strings"
	"time"

	"gitee.com/haodreams/golib/logs"
	"gitee.com/haodreams/libs/config"
	"gitee.com/haodreams/libs/config/iniconf"
	"gitee.com/haodreams/libs/config/jsonconf"
	"gitee.com/haodreams/libs/config/memconf"
	"gitee.com/haodreams/libs/crash"
	"gitee.com/haodreams/libs/ee"
	"gitee.com/haodreams/libs/routine"
	"gitee.com/haodreams/libs/sock"

	_ "time/tzdata"
)

// logDefaultPath 日志的路径
var logDefaultPath = "logs/"

func SetLogPath(path string) {
	if !strings.HasSuffix(path, "/") {
		path += "/"
	}
	logDefaultPath = path
}

// Reset 重设工作目录为程序所在的目录
func Reset() {
	name := logs.GetAppName()

	f, err := os.Stat(logDefaultPath)
	if err == nil && f.IsDir() {
		logs.SetLogger("file", `{"compress":true,"maxsize":20971520,"filename":"`+logDefaultPath+name+`.log"}`) //20M
	} else {
		os.Mkdir(logDefaultPath, 0755)
		logs.SetLogger("file", `{"compress":true,"maxsize":20971520,"filename":"`+logDefaultPath+name+`.log"}`) //20M
	}
	log.SetFlags(log.Lshortfile | log.LstdFlags)
	routine.Start()
	err = crash.Setup(logDefaultPath + "crash.txt")
	if err != nil {
		logs.Error(err)
	}
}

/**
 * @description:初始化基础环境，初始化失败，程序自动退出
 * @param {string} path
 * @return {*}
 */
func Init(configPath ...string) (conf config.Configer) {
	var confPath = ""
	if len(configPath) > 0 {
		confPath = configPath[0]
	}
	Reset()

	conf = LoadConfig(confPath)
	InitDefault(conf)
	return conf
}

func LoadConfig(path string) (conf config.Configer) {
	var err error
	if strings.HasSuffix(path, ".json") {
		conf, err = jsonconf.Load(path)
		if err != nil {
			logs.Exit(err.Error())
			return
		}
	} else {
		if path == "" {
			conf = memconf.NewConf()
		} else {
			conf, err = iniconf.Load(path)
			if err != nil {
				logs.Exit(err.Error())
				return
			}
		}
	}

	return conf
}

// 初始化默认的配置
func InitDefault(conf config.Configer) {
	logs.Init(conf.DefaultString("log_type", "text"), conf.DefaultBool("log_has_color", true))
	if conf.DefaultBool("log_to_console", true) {
		logs.SetLogger(logs.AdapterConsole)
	}
	logs.SetStringLevel(conf.DefaultString("log_level", "info"))

	version := goversion.Info()
	if version == "" {
		logs.Info("无编译版本信息")
	} else {
		logs.Info(version)
	}
}

// 重置工作目录为程序所在的目录
func ResetCurrentDir() {
	binaryPath, _ := filepath.Abs(os.Args[0])
	os.Chdir(filepath.Dir(binaryPath))
}

// 初始化设置
func Setup(confPath string) {
	flag.Parse()
	ResetCurrentDir()
	conf := Init(confPath)
	config.SetSetting(conf)

	// dbname:=config.String("db.host")
	// ....
	// db:= gorm.....
	// c:= dbconf.Load(db,"hn6status")
	// config.SetSetting(c)
}

//main 启动的3行配置
// //启动的默认配置
// conf := env.Init("conf/app.conf")
// gin.SetMode(conf.DefaultString("gin_mode", "release"))
// routine.Start()

/**
 * @description: 创建TCP listener 服务
 * @return {*}
 */
func CreateListenerTCP() (listener *net.TCPListener, err error) {
	for i := 0; i < 5; i++ {
		listener, err = sock.CreateTcpListener(config.DefaultString("http.server", "127.0.0.1:8080"))
		if err != nil {
			time.Sleep(time.Second * 5)
			continue
		}
		return
	}
	return nil, ee.Print(err, logs.Error)
}
