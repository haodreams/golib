/*
 * @Author: Wangjun
 * @Date: 2022-12-30 15:03:06
 * @LastEditTime: 2024-12-14 17:30:39
 * @LastEditors: wangjun haodreams@163.com
 * @Description:
 * @FilePath: \golib\easyredis\redis_test.go
 * hnxr
 */
package easyredis

import (
	"log"
	"testing"

	"gitee.com/haodreams/libs/config"
	"gitee.com/haodreams/libs/config/memconf"
)

func TestHGet(t *testing.T) {
	mem := memconf.NewConf()
	mem.Set("redis_host", "10.202.12.114:6379")
	mem.Set("redis_default_db", "6")

	config.SetSetting(mem)

	redis, err := NewSimpleRedis("")
	if err != nil {
		log.Fatal(err)
	}

	result, err := redis.HGET("APP", "hn6status_recalc_task1")
	if err != nil {
		log.Fatal(err)
	}
	log.Println(result)
}

func TestHGet1(t *testing.T) {
	mem := memconf.NewConf()
	mem.Set("redis_host", "127.0.0.1:26379")
	mem.Set("redis_default_db", "6")
	mem.Set("redis_password", "")
	mem.Set("proxy", "")

	config.SetSetting(mem)

	redis, err := NewSimpleRedis("")
	if err != nil {
		log.Fatal(err)
	}

	msg, err := redis.Set("stock", "00000", "test")
	if err != nil {
		log.Fatal(msg, err)
	}

	result, err := redis.Get("stock", "00000")
	if err != nil {
		log.Fatal(msg, err)
	}
	log.Println(result)
}
