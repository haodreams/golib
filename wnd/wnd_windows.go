/*
 * @Date: 2022-11-12 16:56:26
 * @LastEditors: Wangjun
 * @LastEditTime: 2022-11-12 17:05:01
 * @FilePath: \golib\wnd\wnd_windows.go
 * @Description:
 */
package wnd

import (
	"fmt"
	"syscall"

	"golang.org/x/sys/windows"
)

var libKernel32 = windows.NewLazySystemDLL("Kernel32.dll")
var libuser32 = windows.NewLazySystemDLL("user32.dll")

var getConsoleWindow = libKernel32.NewProc("GetConsoleWindow")
var getSystemMenu = libuser32.NewProc("GetSystemMenu")
var removeMenu = libuser32.NewProc("RemoveMenu")

func RemoveCloseButton() {
	err := getConsoleWindow.Find()
	if err != nil {
		err = fmt.Errorf("function[%s]:%s", getConsoleWindow.Name, err.Error())
		return
	}
	err = getSystemMenu.Find()
	if err != nil {
		err = fmt.Errorf("function[%s]:%s", getSystemMenu.Name, err.Error())
		return
	}
	err = removeMenu.Find()
	if err != nil {
		err = fmt.Errorf("function[%s]:%s", removeMenu.Name, err.Error())
		return
	}
	hwnd, _, _ := syscall.SyscallN(getConsoleWindow.Addr())
	//hwnd = win.HWND(ret)
	hMenu, _, _ := syscall.SyscallN(getSystemMenu.Addr(), hwnd)
	const SC_CLOSE = 0xF060
	const MF_BYCOMMAND = 0
	syscall.SyscallN(removeMenu.Addr(), hMenu, SC_CLOSE, MF_BYCOMMAND)
}
